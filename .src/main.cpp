//============================================================================
// Project Name	: Vector Heap
// Author		: Jose Madureira
// Version		: 2.0
// Copyright	: Public Domain
// Description	: Heap implementation using a vector
//============================================================================

#include <iostream>
#include <cstdlib>	//atoi() string to int
#include <string>
#include <stdlib.h> //rand
#include <time.h>	//time
#include "Heap.h"
using namespace std;

int randKey(int,int);	//randomizes a number

int main() {
	cout << "\t\t##################################"
		 << "\n\t\t##          Vector Heap         ##"
		 << "\n\t\t##   CS 341 - Jose Madureira    ##"
		 << "\n\t\t##################################\n";
	
	Heap *hip= new Heap(); //create a new heap
	Node student;
	
	bool quit= false;
	do {
		cout << "\n  Main Menu"
			 << "\n\t(1) Insert student(s)"
			 << "\n\t(2) Delete top student node"
			 << "\n\t(3) Number of students"
			 << "\n\t(4) Show top student"
			 << "\n\t(5) Show all students"
			 << "\n\t(6) Sort students"
			 << "\n\t(q) Quit program";
		
		string choice, sCtr;
		int ctr=0;
		
		cout << "\n\n>>choice: ";
		getline(cin, choice);

		switch (choice.at(0)) {
			case '1':
				cout << "\nHow many students?  \n";
				
				/*Only allow numeric input*/
				do {  
					cout << ">>number: ";
					getline(cin, sCtr);
				} while (sCtr.at(0) < '1' || sCtr.at(0) > '9');
				
				ctr= atoi(sCtr.c_str());	//convert string to integer
				
				cout<<endl;
				while (ctr > 0) {
					cout << ">>student name: ";
					getline(cin, student.name);
					student.key= randKey(1,99);	//randomize priority key					
					hip->insertNode(student);
					--ctr;
				}
				
				hip->hippyFy();  //re-structure vector into heap format
				
				break;
			case '2':
				hip->deleteTop();
				break;
				
			case '3':
				cout << "\nNumber of students: " << hip->heapSize() << endl;
				break;
				
			case '4':
				hip->peek();
				break;
				
			case '5':
				cout << endl;
				hip->print();
				break;
				
			case '6':
				hip->heapSort();
				break;
				
			case 'q':
				quit= true;
				delete hip;
				
				break;
				
			default:  //input error handler
				do {
					cout << ">>choice: ";
					getline(cin, choice);
					
					if (choice.at(0) == 'q') {  //exit program
						quit= true;
						break;
					}
				} while ((choice.at(0) < '1' || choice.at(0) > '6'));
				
				break;
		}
		
	} while (quit == false);
	
	cout << "\n\t\tGOODBYE!!!\n";
	
	return 0;
}
/*Randomize an integer between two bounds*/
int randKey(int low, int high){
	//seed
 	srand (time(NULL));
	
	//return random number
	return rand() % high + low;
}

