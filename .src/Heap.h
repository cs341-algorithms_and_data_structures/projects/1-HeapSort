/*
 * Heap.h
 *
 *  Created on: Feb 13, 2017
 *      Author: Jose Madureira
 */

#ifndef HEAP_H_
#define HEAP_H_

#include <iostream>
#include <vector>
#include <algorithm> //vector heap operations
using namespace std;

struct Node {
		friend class Heap;
		string name;
		int key;
};

/*Custom Node String Comparison Rule*/
struct CompareKeys {
	bool operator()(const Node& first, const Node& second) const {
		return first.key < second.key;  //compare priority keys
	}
};

/*Heap class*/
class Heap {
	public:
		Heap();
		~Heap();
		void insertNode(const Node&);	//push node to heap
		void hippyFy();  				//re-arrange heap structure
		void heapSort();  				//sort the heap
		size_t heapSize();  			//return heap size
		void peek();					//Show the top node
		void print();  					//show all nodes
		void deleteTop();  				//pop top node
		
	private:
		static vector <Node> vHeap;
		static int nodeCount;
};

#endif /* HEAP_H_ */
