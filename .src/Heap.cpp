/*
 * Heap.cpp
 *
 *  Created on: Feb 13, 2017
 *      Author: Jose Madureira
 */

#include "Heap.h"

/*Initizalize Static Members*/
int Heap::nodeCount=0;
vector <Node> Heap::vHeap;

/*Constructor*/
Heap::Heap() {
	cout<<"\n#Heap staus: created a heap\n";
}

/*Destructor*/
Heap::~Heap() {
	cout << "\n#Heap status: deleted the heap\n";
}

/*Inserts Node Into Heap*/
void Heap::insertNode(const Node& n) {
	if (nodeCount==0) {  //leave first vector node empty
		vHeap.push_back(Node());
		vHeap.push_back(n);
	} else {
		vHeap.push_back(n);
	}
	++nodeCount;
}

/* Build Max-heap by Comparing Priority Keys*/
void Heap::hippyFy() {
	if (nodeCount == 0)
		cout << "/!\\ Empty Heap /!\\" << endl;
	else
		make_heap(vHeap.begin() + 1, vHeap.end(), CompareKeys());
}

/*Sort the Heap Using min-sort*/
void Heap::heapSort() {
	if (nodeCount == 0)
		cout << "\n/!\\ Empty Heap /!\\" << endl;
	else
		sort_heap(vHeap.begin() + 1, vHeap.end(), CompareKeys());
}

/*Return the Number of Nodes*/
size_t Heap::heapSize() {
	return nodeCount;
}

/*Show the Student at the Root*/
void Heap::peek() {
	if (nodeCount == 0)
		cout << "\n/!\\ Empty Heap /!\\" << endl;
	else
		printf("\nTop student\n\t Key: %02d Name: %s \n ", vHeap.at(1).key,
		       vHeap.at(1).name.c_str());
}

/*Show all Heap Nodes*/
void Heap::print() {
	int ctr= 0;

	if (nodeCount == 0)
		cout << "/!\\ Empty Heap /!\\" << endl;
	else {
		for (vector<Node>::iterator it = vHeap.begin(); it != vHeap.end(); ++it) {
			if (0 != ctr++)  //skip empty first node
				printf(" Key: %02d Name: %s \n", it->key, it->name.c_str());
		}
	}
}

/*Remove the Root Node*/
void Heap::deleteTop() {
	if(nodeCount != 0) {
		pop_heap(vHeap.begin()+1, vHeap.end(), CompareKeys());
		vHeap.pop_back();

		hippyFy();  //restructure max-heap
		--nodeCount;

		cout<<"\n Deleted top student, number of students: "
		    <<heapSize()<<endl;
	} else {
		cout << "\n/!\\ Empty Heap /!\\" << endl;
	}
}
